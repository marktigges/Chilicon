package ca.tigges.Chilicon;

import android.os.AsyncTask;

abstract class ChiliconIOTask extends AsyncTask<Void,Void,ChiliconSession>
{
    protected ChiliconIORequestor mRequestor;
    private ChiliconSession mSession;

    protected ChiliconIOTask(ChiliconIORequestor requestor)
    {
        mRequestor = requestor;
    }

    public void useSession(ChiliconSession session)
    {
        mSession = session;
    }

    @Override
    protected ChiliconSession doInBackground(Void... params)
    {
        if(mSession==null)
        {
            mSession = new ChiliconSession(mRequestor.getPreferences());
        }
        if(mSession.isValid())
        {
            doIO(mSession);
        }
        return mSession;
    }

    protected abstract void doIO(ChiliconSession session);

}

class ChiliconOwnerUpdate extends ChiliconIOTask
{
    public ChiliconOwnerUpdate(ChiliconIORequestor requestor)
    {
        super(requestor);
    }

    @Override
    protected void doIO(ChiliconSession session)
    {
        session.fetchOwnerUpdate();
    }

    @Override
    protected void onPostExecute(ChiliconSession session)
    {
        super.onPostExecute(session);
        mRequestor.newOwnerUpdate(session);
    }
}

class ChiliconBargraphData extends ChiliconIOTask
{
    public ChiliconBargraphData(ChiliconIORequestor requestor)
    {
        super(requestor);
    }

    @Override
    protected void doIO(ChiliconSession session)
    {
        session.fetchBarGraphData();
    }

    @Override
    protected void onPostExecute(ChiliconSession session)
    {
        super.onPostExecute(session);
        mRequestor.newBargraphData(session);
    }
}

class ChiliconInverterData extends ChiliconIOTask
{
    public ChiliconInverterData(ChiliconIORequestor requestor)
    {
        super(requestor);
    }

    @Override
    protected void doIO(ChiliconSession session)
    {
        session.fetchInverterData();
    }

    @Override
    protected void onPostExecute(ChiliconSession session)
    {
        super.onPostExecute(session);
        mRequestor.newInverterData(session);
    }

}
