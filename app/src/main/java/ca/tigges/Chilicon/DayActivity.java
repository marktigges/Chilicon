package ca.tigges.Chilicon;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DayActivity extends ChiliconActivity
{
    ChiliconSession mOriginalSession;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day);

        if (mSession != null)
        {
            //DayCurveView curve = findViewById(R.id.dayCurve);
            //curve.setSession(mSession, this);
            String today = new SimpleDateFormat("E MMM dd, yyyy", Locale.getDefault()).format(mSession.getQueryDate());
            setTitle(today);
            mOriginalSession = mSession;
        }

        // this async task disregards and existing session passed in through extra's
        // we ignore it because, the day activity allows swiping through past days,
        // we don't want to change the date on the original session, so we create a
        // second.
        ChiliconIOTask task = new ChiliconOwnerUpdate(this);
        task.execute((Void) null);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        DayCurveView curve = findViewById(R.id.dayCurve);
        switch (action)
        {
            case MotionEvent.ACTION_MOVE:
            {
                curve.onTouch(event.getX(),event.getY());
            }
            break;
            case MotionEvent.ACTION_CANCEL:
            {
                curve.onTouch(-1,-1);
            }
            break;
            case MotionEvent.ACTION_UP:
            {
                curve.onTouch(-1,-1);
            }
            break;
        }
        return true;
    }

    @Override
    protected ChiliconSession getMainSession()
    {
        return mOriginalSession;
    }

    @Override
    public void newOwnerUpdate(ChiliconSession session)
    {
        super.newOwnerUpdate(session);
        String today = new SimpleDateFormat("E MMM dd, yyyy", Locale.getDefault()).format(session.getQueryDate());
        setTitle(today);
        DayCurveView curve = findViewById(R.id.dayCurve);
        curve.setSession(session, this);
    }
}