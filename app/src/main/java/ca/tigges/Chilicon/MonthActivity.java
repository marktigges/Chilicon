package ca.tigges.Chilicon;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MonthActivity extends BargraphActivity
{
    public void newBargraphData(ChiliconSession session)
    {
        super.newBargraphData(session);
        if(session.getMonth()!=null && session.getMonth().length==31)
        {
            BargraphView bargraph = findViewById(R.id.bargraphView);
            float[] month = new float[31];
            for (int i = 0; i < 30; i++)
            {
                month[i] = session.getMonth()[i];
            }
            month[30] = session.getGeneratedToday();
            bargraph.setData(month);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -30);
            String then = new SimpleDateFormat("MMM dd", Locale.getDefault()).format(calendar.getTime());
            String now = new SimpleDateFormat("MMM dd", Locale.getDefault()).format(session.getQueryDate());
            setTitle(then + " -> " + now);
        }
    }

    @Override
    public String getBarDateText(int bar)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, bar-30);
        return new SimpleDateFormat("MMM dd", Locale.getDefault()).format(calendar.getTime());
    }
}
