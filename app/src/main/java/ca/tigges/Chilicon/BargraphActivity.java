package ca.tigges.Chilicon;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class BargraphActivity extends ChiliconActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bargraph);
        ChiliconIOTask task = new ChiliconBargraphData(this);
        task.useSession(mSession);
        task.execute((Void) null);
        task = new ChiliconOwnerUpdate(this);
        task.useSession(mSession);
        task.execute((Void)null);
        BargraphView bargraphView = (BargraphView)findViewById(R.id.bargraphView);

        bargraphView.setOnTouchListener(new OnSwipeTouchListener(this)
        {
            public void onSwipeDown()
            {
                ChiliconIOTask task = new ChiliconOwnerUpdate(mContext);
                task.useSession(mSession);
                task.execute((Void)null);
            }
        });
    }

    public void newOwnerUpdate(ChiliconSession session)
    {
        super.newOwnerUpdate(session);
        newBargraphData(session);
    }

    public abstract String getBarDateText(int bar);

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        BargraphView graph = findViewById(R.id.bargraphView);
        switch (action)
        {
            case MotionEvent.ACTION_MOVE:
            {
                graph.onTouch(event.getX(),event.getY());
            }
            break;
            case MotionEvent.ACTION_CANCEL:
            {
                graph.onTouch(-1,-1);
            }
            break;
            case MotionEvent.ACTION_UP:
            {
                graph.onTouch(-1,-1);
            }
            break;
        }
        return true;
    }
}


