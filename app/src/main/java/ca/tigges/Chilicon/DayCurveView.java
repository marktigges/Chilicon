package ca.tigges.Chilicon;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import java.util.Date;

public class DayCurveView extends WattGridView
{
    ChiliconIORequestor mRequestor;
    ChiliconSession mSession;

    Paint mTextPaint, mCurvePaint;
    int mStartTime, mEndTime;

    int mTouchMinutes;
    float mTouchKW;

    public DayCurveView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        mSession = null;
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextSize(50.0f);
        mCurvePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCurvePaint.setStyle(Paint.Style.STROKE);
        mCurvePaint.setStrokeWidth(2);

        setOnTouchListener(new OnSwipeTouchListener((ChiliconActivity) context)
        {
            public boolean getAllowDayChange()
            {
                return getContext().getResources().getBoolean(R.bool.allow_day_change);
            }

            public void onSwipeRight()
            {
                if (getAllowDayChange() && mSession != null)
                {
                    mSession.backOneDay();
                    ChiliconIOTask task = new ChiliconOwnerUpdate(mRequestor);
                    task.useSession(mSession);
                    task.execute((Void) null);
                }
            }

            public void onSwipeLeft()
            {
                if (getAllowDayChange() && mSession != null)
                {
                    if(mSession.forwardOneDay())
                    {
                        ChiliconIOTask task = new ChiliconOwnerUpdate(mRequestor);
                        task.useSession(mSession);
                        task.execute((Void) null);
                    }
                }
            }

            public void onSwipeDown()
            {
                // todo ... should really only do this if curent
                // query date is today.
                if (mSession != null)
                {
                    mSession.setDay(new Date());
                    ChiliconIOTask task = new ChiliconOwnerUpdate(mRequestor);
                    task.useSession(mSession);
                    task.execute((Void) null);
                }
            }

        });
    }

    public void setSession(ChiliconSession session, ChiliconIORequestor requestor)
    {
        mSession = session;
        mRequestor = requestor;

        init();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        // make sure that we have a session
        if (mSession != null && mSession.isValid())
        {
            float[] today = mSession.getToday();

            // draw line segments for each interval, todo ... change this into cubic curve
            for (int i = mStartTime; i < mEndTime; i++)
            {
                line(canvas, (float) i, today[i], (float) i + 1, today[i + 1]);
            }

            float w = getMaxKW();
            float k = 1;
            String units = "Watts";
            if (getMaxKW() > 1.000f)
            {
                units = "KW";
            }
            else
            {
                k = 1000.0f;
            }
            if(mTouchMinutes<=0)
            {
                canvas.drawText(String.format("%.2f KWh", mSession.getGeneratedToday()), 0, 51, mTextPaint);
                canvas.drawText(String.format("%.2f %s max", w * k, units), 0, 102, mTextPaint);
            }
            else
            {
                int h = mTouchMinutes / 60;
                int m = mTouchMinutes % 60;
                float tk = 1.0f;
                String tunits = "Watts";
                if(mTouchKW>1000.0f)
                {
                    tk = 1.0f/1000.f;
                    tunits = "KW";
                }
                canvas.drawText(String.format("%d:%02d : %.2f %s",h,m,mTouchKW*tk,tunits),0,51,mTextPaint);
            }
            drawUnits(canvas, units, k);
        }
    }

    public void onTouch(float x, float y)
    {
        if(x<0 && y<0)
        {
            mTouchMinutes = -1;
        }
        else if(mSession!=null)
        {
            int t = mStartTime + (int)((x/getWidth())*(mEndTime-mStartTime));
            mTouchMinutes = t*5;
            mTouchKW = Math.max(0,mSession.getToday()[t]);
        }
        invalidate();
    }

    // initializes the prerequisite info required for rendering.
    private void init()
    {
        mStartTime = mEndTime = -1;
        float max = 0;
        float[] today = mSession.getToday();

        // make sure that there is data in the session
        if (today!=null && today.length == 288)
        {

            // first find the two intervals where there was at least one active inverter
            // and the range of watts generated

            for (int i = 0; i < 288; i++)
            {
                if (mStartTime == -1 && today[i] != -1) mStartTime = i;
                if (mEndTime == -1 && today[i] == -1 && mStartTime!=-1) mEndTime = i - 1;
                max = Math.max(today[i],max);
            }

            // day time data is in watts.
            max = max / 1000.0f;
        }
        setMaxKW(max);
        mTouchMinutes = 0;
    }

    private void line(Canvas canvas, float x0, float y0, float x1, float y1)
    {
        // normalize to 0..1
        x0 = (x0-mStartTime) / (mEndTime-mStartTime);
        x1 = (x1-mStartTime) / (mEndTime-mStartTime);

        // draw in screen space.
        canvas.drawLine(x0*getWidth(),getYFromWatts(y0), x1*getWidth(), getYFromWatts(y1), mCurvePaint);
    }
}
