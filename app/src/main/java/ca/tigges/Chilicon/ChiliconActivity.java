package ca.tigges.Chilicon;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class ChiliconActivity extends AppCompatActivity implements ChiliconIORequestor
{
    protected ChiliconSession mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle!=null)
        {
            mSession = (ChiliconSession) bundle.getSerializable("session");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        try
        {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent = null;
        Bundle options = null;
        int code=-1; // replace these with resources
        switch(id)
        {
            case R.id.action_today:
                intent = new Intent(this,DayActivity.class);
                break;
            case R.id.action_week:
                intent = new Intent(this,WeekActivity.class);
                break;
            case R.id.action_month:
                intent = new Intent(this,MonthActivity.class);
                break;
            case R.id.action_year:
                intent = new Intent(this,YearActivity.class);
                break;
            case R.id.action_inverters:
                intent = new Intent(this,InvertersActivity.class);
                break;
            case R.id.action_reset:
                intent = new Intent(this,MainActivity.class);
                break;
            case R.id.action_login:
                intent = new Intent(this,LoginActivity.class);
                code = 0;
                break;
        }
        if(intent!=null)
        {
            if (options == null) options = new Bundle();
            options.putSerializable("session",getMainSession());
            intent.putExtras(options);
            if(code!=-1)
            {
                startActivityForResult(intent, code, null);
            }
            else
            {
                startActivity(intent,null);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    protected ChiliconSession getMainSession()
    {
        return mSession;
    }

    public SharedPreferences getPreferences()
    {
        return PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    public void newOwnerUpdate(ChiliconSession session)
    {
        cacheSession(session);
    }

    public void newBargraphData(ChiliconSession session)
    {
        cacheSession(session);
    }

    public void newInverterData(ChiliconSession session)
    {
        cacheSession(session);
    }

    protected void cacheSession(ChiliconSession session)
    {
        mSession = session;
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString("sessionid", session.getSessionId());
        editor.putString("location", session.getLocation());
        editor.commit();
    }

}
