package ca.tigges.Chilicon;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import java.util.Date;

public class BargraphView extends WattGridView
{
    protected float[] mData;

    Paint mBarPaint;

    int mTouchBar = -1;

    public BargraphView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        mBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBarPaint.setStyle(Paint.Style.FILL);
        mBarPaint.setColor(Color.rgb(100, 160, 20));
        mBarPaint.setStrokeWidth(1);
    }

    public void setData(float[] data)
    {
        mData = data;
        float max = 0.0f;
        if(mData!=null)
        {
            for (int i = 0; i < mData.length; i++)
            {
                max = Math.max(max, mData[i]);
            }
        }
        setMaxKW(max);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        if(mData!=null)
        {
            float barw = getWidth() / mData.length;  // width of a single bar in the graph
            for(int i=0; i<mData.length; i++)
            {
                mBarPaint.setAlpha(mTouchBar<0 || i==mTouchBar ? 255 : 128);
                canvas.drawRect(barw*i+10,getYFromKW(mData[i]),barw*(i+1)-10,getHeight(),mBarPaint);
            }
            if(mTouchBar<0 || mTouchBar>=mData.length)
            {
                drawUnits(canvas, "KWh", 1.0f);
            }
            else
            {
                canvas.drawText(((BargraphActivity)getContext()).getBarDateText(mTouchBar),0,51,mTextPaint);
                canvas.drawText(String.format("%.2f KWh",mData[mTouchBar]),0,102,mTextPaint);

            }
        }
    }

    public void onTouch(float x, float y)
    {
        mTouchBar = (x>0.0f) ? (int)(mData.length * x / getWidth()) : -1;
        invalidate();
    }
}
