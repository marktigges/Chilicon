package ca.tigges.Chilicon;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class YearActivity extends BargraphActivity
{
    public void newBargraphData(ChiliconSession session)
    {
        super.newBargraphData(session);
        if (session != null)
        {
            BargraphView bargraph = findViewById(R.id.bargraphView);
            bargraph.setData(session.getYear());
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -11);
            String then = new SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(calendar.getTime());
            String now =  new SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(session.getQueryDate());
            setTitle(then + " -> " + now);
        }
    }

    @Override
    public String getBarDateText(int bar)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, bar-11);
        return new SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(calendar.getTime());
    }
}
