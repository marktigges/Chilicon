package ca.tigges.Chilicon;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Contacts;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class InvertersActivity extends ChiliconActivity
{
    ArrayAdapter<String> adapter;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inverters);
        mListView = (ListView) findViewById(R.id.invertersView);

        if(mSession!=null)
        {
            ChiliconInverterData data = new ChiliconInverterData(this);
            data.useSession(mSession);
            data.execute((Void)null);
        }

        mListView.setOnTouchListener(new OnSwipeTouchListener(this)
        {
            public void onSwipeDown()
            {
                ChiliconInverterData data = new ChiliconInverterData(mContext);
                data.useSession(mSession);
                data.execute((Void)null);
            }
        });
    }

    @Override
    public void newInverterData(ChiliconSession session)
    {
        InvertersAdapter data = new InvertersAdapter(this, session.getInverters());
        mListView.setAdapter(data);
        TextView status = findViewById(R.id.invertersOverview);
        status.setText(String.format("%d on : %d off : %.3f average power",data.getCountOn(),data.getCountOff(),data.getAveragePower()));

    }
}

class InvertersAdapter extends BaseAdapter
{
    Context mContext;
    LayoutInflater mInflater;
    List<Integer> mIds;
    List<Float> mMaximums;
    List<Float> mPowers;
    List<Float> mTotals;
    Hashtable<Integer,ArrayList<Pair<Integer,Float>>> mInverters;

    public InvertersAdapter(ChiliconActivity context, Hashtable<Integer,ArrayList<Pair<Integer,Float>>> inverters)
    {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mInverters = inverters;
        mIds = Collections.list(inverters.keys());

        // fetch from preferences all of the id's that have ever reported a value.
        SharedPreferences preferences = context.getPreferences();
        Set<String> knownInverterSet = preferences.getStringSet("knowninverters", null);
        for(Iterator<String> i=knownInverterSet.iterator(); i.hasNext(); )
        {
            Integer id = Integer.parseInt(i.next());
            if(!mInverters.containsKey(id))
            {
                mIds.add(id);
            }
        }

        // check to see if we're seeing a new inverter, if yes,
        // write its id to the set of known inverters
        if(knownInverterSet.size()!=mInverters.size())
        {
            knownInverterSet.clear();
            for(int i=0; i<mIds.size(); i++)
            {
                knownInverterSet.add(String.format("%d",mIds.get(i)));
            }
            SharedPreferences.Editor editor = preferences.edit();
            editor.putStringSet("knowninverters",knownInverterSet);
            editor.commit();
        }

        Collections.sort(mIds);
        mMaximums = new ArrayList<Float>();
        mPowers = new ArrayList<Float>();
        mTotals = new ArrayList<Float>();

        for(int i=0; i<mIds.size(); i++)
        {
            float maxp = 0.0f;
            float power = 0.0f;
            float total = 0.0f;
            if(mInverters.containsKey(mIds.get(i)))
            {
                List<Pair<Integer, Float>> p = mInverters.get(mIds.get(i));
                for (int j = 0; j < p.size(); j++)
                {
                    maxp = Math.max(maxp, p.get(j).second);
                    if(p.get(j).second>0.0f)
                    {
                        power = p.get(j).second;
                        total += p.get(j).second;
                    }
                }
            }
            mMaximums.add(maxp);
            mPowers.add(power);
            mTotals.add(total);
        }
    }

    @Override
    public int getCount()
    {
        return mInverters.size();
    }

    @Override
    public Object getItem(int position)
    {
        return mMaximums.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return mIds.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View inverterView = mInflater.inflate(R.layout.inverter_item, parent, false);

        TextView power = inverterView.findViewById(R.id.inverterPower);
        power.setText(String.format("%.3f",mPowers.get(position)));

        TextView maxpower = inverterView.findViewById(R.id.inverterMaxPower);
        maxpower.setText(String.format(": %.3f",mMaximums.get(position)));

        TextView totalpower = inverterView.findViewById(R.id.inverterTotalPower);
        totalpower.setText(String.format(": %.3f",mTotals.get(position)));

        ImageView status = inverterView.findViewById(R.id.inverterStatus);
        if(mMaximums.get(position)>0.0f)
        {
            status.setImageResource(R.mipmap.green_inverter);
        }
        else
        {
            status.setImageResource(R.mipmap.red_inverter);
        }
        return inverterView;
    }

    public int getCountOn()
    {
        int count = 0;
        for(int i=0; i<mIds.size(); i++)
        {
            if(mPowers.get(i)>0.0f)
            {
                count += 1;
            }
        }
        return count;
    }

    public int getCountOff()
    {
        return mIds.size()-getCountOn();
    }

    public float getAveragePower()
    {
        float power = 0.0f;
        int count = 0;
        for(int i=0; i<mIds.size(); i++)
        {
            if(mPowers.get(i)>0.0f)
            {
                count += 1;
                power += mPowers.get(i);
            }
        }
        power = power / (float)count;
        return power;
    }
}