package ca.tigges.Chilicon;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;


public class MainActivity extends ChiliconActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        ChiliconSession.UseTestData = getResources().getBoolean(R.bool.use_test_data);

        super.onCreate(savedInstanceState);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(!preferences.contains("username") || !preferences.contains("password") || !preferences.contains("sessionid"))
        {
            Intent intent = new Intent(this, LoginActivity.class );
            Bundle options = new Bundle();
            startActivityForResult(intent,0, options);
        }
        else
        {
            createSession();
        }
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            if(mSession!=null) // if we came back from a today view and refreshed there, need to force a redisplay of the dial.
            {
                DialView dialView = findViewById(R.id.dialView);
                dialView.setSession(mSession,this);
            }
            if (requestCode==0 && data.hasExtra("username"))
            {
                SharedPreferences.Editor preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                preferences.putString("username",data.getStringExtra("username"));
                preferences.putString("password",data.getStringExtra("password"));
                preferences.commit();
                createSession();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        boolean result = true;
        if(item.getItemId()==R.id.action_reset)
        {
            createSession();
        }
        else
        {
            result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    private void createSession()
    {
        ChiliconIOTask task = new ChiliconOwnerUpdate(this);
        task.execute((Void) null);

        // task = new ChiliconInverterData(this);
        // task.execute();
    }

    @Override
    public void newOwnerUpdate(ChiliconSession session)
    {
        cacheSession(session);

        try
        {
            TextView textView;

            String today = new SimpleDateFormat("E MMM dd, yyyy", Locale.getDefault()).format(session.getQueryDate());

            textView = findViewById(R.id.dateShown);
            textView.setText(today);

            float x = session.getLifetimeEnergy();
            String units = "KWh";
            if(x>1000)
            {
                units = "MWh";
                x /= 1000.0f;
            }
            textView = findViewById(R.id.lifetimeEnergy);
            textView.setText(String.format("%.1f %s",x,units));

            DialView dialView = findViewById(R.id.dialView);
            dialView.setSession(session,this);
        }
        catch(Exception e)
        {
            String message = e.getMessage();
            System.out.println(message);
        }
    }

 }
