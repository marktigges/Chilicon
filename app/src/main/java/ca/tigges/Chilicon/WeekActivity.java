package ca.tigges.Chilicon;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class WeekActivity extends BargraphActivity
{
    public void newBargraphData(ChiliconSession session)
    {
        super.newBargraphData(session);
        if (session.getWeek() != null && session.getWeek().length == 7)
        {
            BargraphView bargraph = findViewById(R.id.bargraphView);
            float[] week = new float[7];
            for (int i = 0; i < 6; i++)
            {
                week[i] = session.getWeek()[i];
            }
            week[6] = session.getGeneratedToday();
            bargraph.setData(week);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -6);
            String then = new SimpleDateFormat("MMM dd", Locale.getDefault()).format(calendar.getTime());
            String now = new SimpleDateFormat("MMM dd", Locale.getDefault()).format(session.getQueryDate());
            setTitle(then + " -> " + now);
        }
    }

    @Override
    public String getBarDateText(int bar)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, bar-6);
        return new SimpleDateFormat("MMM dd", Locale.getDefault()).format(calendar.getTime());
    }
}
