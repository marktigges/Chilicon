package ca.tigges.Chilicon;


import android.content.SharedPreferences;

public interface ChiliconIORequestor
{
    SharedPreferences getPreferences();

    void newOwnerUpdate(ChiliconSession session);

    void newBargraphData(ChiliconSession session);

    void newInverterData(ChiliconSession session);
}
