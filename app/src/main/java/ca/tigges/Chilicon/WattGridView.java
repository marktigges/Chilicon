package ca.tigges.Chilicon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;


public class WattGridView extends View
{
    static final float BarNum = 5.0f;

    protected Paint mTextPaint;
    Paint mLightPaint, mDarkPaint;
    float mMaxKW,mBarKW,mTopKW;

    public WattGridView(Context context, AttributeSet attrs)
    {
        super(context,attrs);

        mMaxKW = 0.0f;

        mLightPaint = new Paint();
        mDarkPaint = new Paint();

        mLightPaint.setStyle(Paint.Style.FILL);
        mLightPaint.setColor(Color.rgb(220, 220, 220));
        mLightPaint.setStrokeWidth(1);
        mDarkPaint.setStyle(Paint.Style.FILL);
        mDarkPaint.setColor(Color.rgb(192, 192, 192));
        mDarkPaint.setStrokeWidth(1);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextSize(50.0f);
    }

    public float getMaxKW()
    {
        return mMaxKW;
    }

    public void setMaxKW(float kw)
    {
        mMaxKW = kw;

        // figure out the number of watts in each bar.  there's got to be a better way to do this.
        float barkw = mMaxKW / 5;
        mTopKW = (float)Math.floor(mMaxKW+barkw);
        if(mTopKW==0.0f || mTopKW<mMaxKW)
        {
            mTopKW = (float)Math.ceil(mMaxKW+barkw);
        }
        if(barkw>100.000f)
        {
            barkw = 100.000f;
        }
        else if(barkw>50.000f)
        {
            barkw = 50.000f;
        }
        else if(barkw>25.000f)
        {
            barkw = 25.000f;
        }
        else if(barkw>10.000f)
        {
            barkw = 10.000f;
        }
        else if(barkw>5.000f)
        {
            barkw = 5.000f;
        }
        else if(barkw>2.500f)
        {
            barkw = 2.500f;
        }
        else if(barkw>1.000f)
        {
            barkw = 1.000f;
        }
        else if(barkw>.500f)
        {
            barkw = .500f;
        }
        else if(barkw>.250f)
        {
            barkw = .250f;
        }
        else
        {
            barkw = .100f;
        }
        mBarKW = barkw;
    }

    public int getScreenMaxWatts()
    {
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (((Activity)getContext()).getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        return size.y-actionBarHeight;
    }

    public float getYFromWatts(float w)
    {
        return getYFromKW(w/1000.0f);
    }

    public float getYFromKW(float kw)
    {
        float kwpp = getHeight()/mTopKW;
        return getHeight()-kw*kwpp;
    }

    public float getKWFromY(float y)
    {
        return (-y+getHeight()) / (getHeight()/mTopKW);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if(mMaxKW>0)
        {
            String units = "kw";
            float textk = 1.0f;

            if(mBarKW<1)
            {
                units = "w";
                textk = 1000.0f;
            }

            Paint p = mDarkPaint;

            float amount = 0;
            while(amount<(mMaxKW+mBarKW))
            {
                canvas.drawRect(0,getYFromKW(amount),getWidth(),getYFromKW(amount+mBarKW),p);
                if(p==mDarkPaint) p = mLightPaint; else p = mDarkPaint;
                amount+=mBarKW;
            }
        }
    }

    protected void drawUnits(Canvas canvas, String units, float k)
    {
        float amount = mBarKW;
        while(amount<(mMaxKW+mBarKW))
        {
            float v = amount*k;
            if(v==(int)v)
            {
                canvas.drawText(String.format("%d %s", (int) v, units), 2, getYFromKW(amount), mTextPaint);
            }
            else
            {
                float d = v-(int)v;
                if(d==0.1f || d==0.5f)
                {
                    canvas.drawText(String.format("%.1f %s", v, units), 2, getYFromKW(amount), mTextPaint);
                }
                else
                {
                    canvas.drawText(String.format("%.2f %s", v, units), 2, getYFromKW(amount), mTextPaint);
                }
            }
            amount+=mBarKW;
        }
    }
}
