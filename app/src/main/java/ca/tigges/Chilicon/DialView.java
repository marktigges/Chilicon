package ca.tigges.Chilicon;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.Display;

import java.util.Date;

public class DialView extends AppCompatImageView
{
    ChiliconIORequestor mRequestor;
    ChiliconSession mSession;

    Paint mTextPaint,mCurvePaint,mNeedlePaint;
    Path mNeedle;

    ChiliconActivity mActivity;

    public DialView(Context context, AttributeSet attrs)
    {
        super(context,attrs);

        mActivity = (ChiliconActivity) context;

        Display display = mActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int sz = Math.min(size.x,size.y);

        Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.dial),sz,sz,true);

        setImageBitmap(bmp);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextSize(sz*0.05f);
        mTextPaint.setTypeface(mTextPaint.setTypeface(Typeface.createFromAsset(mActivity.getAssets(),"fonts/DS-DIGII.TTF")));

        mCurvePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCurvePaint.setStyle(Paint.Style.STROKE);

        mNeedlePaint = new Paint();
        mNeedlePaint.setColor(Color.rgb(240, 75, 0));
        mNeedlePaint.setStyle(Paint.Style.FILL);

        mNeedle = new Path();
        mNeedle.reset();
        mNeedle.moveTo(-0.07f,0);
        mNeedle.lineTo(0.01f, 0.05f);
        mNeedle.lineTo(1.0f,  0.01f);
        mNeedle.lineTo(1.0f, -0.01f);
        mNeedle.lineTo(0.01f,-0.05f);
        mNeedle.close();

        setOnTouchListener(new OnSwipeTouchListener(mActivity)
        {
            public void onSwipeDown()
            {
                if (mSession != null)
                {
                    mSession.setDay(new Date());
                    ChiliconIOTask task = new ChiliconOwnerUpdate(mRequestor);
                    task.useSession(mSession);
                    task.execute((Void) null);
                }
            }
        });
    }

    public void setSession(ChiliconSession session, ChiliconIORequestor requestor)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext());
        float maxproduction = preferences.getFloat("maxproduction",0.0f);

        mSession = session;
        mRequestor = requestor;

        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat("maxproduction", Math.max(maxproduction,mSession.getCurrentProduction()));
        editor.apply();

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        if (mSession != null)
        {
            //Display display = mActivity.getWindowManager().getDefaultDisplay();
            //Point size = new Point();
            //display.getSize(size);

            float current = mSession.getCurrentProduction();

            int sz = Math.min(getWidth(), getHeight());                            // general global scale factor
            float cx = getWidth()/2.0f, cy = getHeight()/2.0f;//-sz*.12f;           // centre of the dial

            // draw the text for the current kw being produced
            String kw = String.format("%.2f %s", current, "KW");
            Rect bounds = new Rect();
            mTextPaint.getTextBounds(kw, 0, kw.length(), bounds);
            canvas.drawText(kw, cx - bounds.width() / 2.0f, cy+ sz * 0.35f, mTextPaint);

            // draw the needle in the dial
            Path needle = new Path();
            Matrix T = new Matrix();
            float max = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext()).getFloat("maxproduction",0.0f);
            if(max==0) max = 1.0f;
            max = (float)Math.ceil(max);  // round up to the nearest KW
            float k = current/max;
            T.postScale(sz*0.35f, sz*0.35f);
            T.postRotate(-225.0f+k*270.f);  // from min watts to max watts.
            T.postTranslate(cx,cy);
            mNeedle.transform(T,needle);
            canvas.drawPath(needle,mNeedlePaint);

            // draw the numbers for the range of the needle
            kw = String.format("%d",(int)max);
            mTextPaint.getTextBounds(kw, 0, kw.length(), bounds);
            canvas.drawText("0",cx-sz*.25f,cy + sz * 0.325f, mTextPaint);
            canvas.drawText(kw,cx+sz*.23f-bounds.width(),cy + sz * 0.325f, mTextPaint);
        }
    }
}
